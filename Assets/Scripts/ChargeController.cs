﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChargeController : MonoBehaviour
{

    public Transform[] charges;
    public TEST2PlayerController player;

    void Start() {
        charges = GetComponentsInChildren<Transform>();
    }
    
    void Update() {
            for (int i = 1; i < charges.Length; i++) { //skipping over the first element as it is this object itself
                if(i >= player.numCharges + 1) {
                charges[i].gameObject.SetActive(false);
                } else {
                charges[i].gameObject.SetActive(true);
                }
            }
        }
}

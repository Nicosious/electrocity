﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyAI : MonoBehaviour {

    public float moveSpeed;
    public GameObject startPoint;
    public GameObject endPoint;
    public GameObject playerTarget;
    bool faceEnd = true;
    bool PlayerInRange;

    void Start () {
        transform.position = startPoint.transform.position;
        PlayerInRange = false;

    }
	
	// Rotate Direction using boolean
	void FixedUpdate () {
        checkFaceDirection();
        if (!PlayerInRange) {
            if (faceEnd) {
                DirectionControl(endPoint);
            } else {
                DirectionControl(startPoint);
            }
        } else {
            DirectionControl(playerTarget);
        }
    }

    //controls enemy position & rotation: modified from https://www.youtube.com/watch?v=dp3lZUDij6Y 
    void DirectionControl(GameObject target){
        Vector3 targetRot = new Vector3(target.transform.position.x, target.transform.position.y, transform.rotation.eulerAngles.z);
        
        transform.LookAt(targetRot, Vector3.back);
        transform.position = Vector3.MoveTowards(transform.position, target.transform.position, moveSpeed * Time.deltaTime);
    }

    void checkFaceDirection() {
        if (transform.position == startPoint.transform.position) {
            faceEnd = true;
        } else if (transform.position == endPoint.transform.position) {
            faceEnd = false;
        }
    }

    //Detect when player is in attack range
    void OnTriggerEnter2D(Collider2D other) {
        if (other.gameObject.tag == "Player") {
            PlayerInRange = true;
        }
    }

    //Detect when player has left attack range
    void OnTriggerExit2D(Collider2D other) {
        if (other.gameObject.tag == "Player") {
            PlayerInRange = false;
        }
    }
}

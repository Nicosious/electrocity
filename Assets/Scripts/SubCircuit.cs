﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SubCircuit : MonoBehaviour {

    public ConductiveObject[] wires;
    void Start() {
        wires = GetComponentsInChildren<ConductiveObject>();
    }

    //Powers all wires
    public void powerWires() {
        for (int i = 0; i < wires.Length; i++) {
            wires[i].setPowered(true);
        }
    }

    //Depowers all wires
    public void depowerWires() {
        for (int i = 0; i < wires.Length; i++) {
            wires[i].setPowered(false);
        }
    }
}

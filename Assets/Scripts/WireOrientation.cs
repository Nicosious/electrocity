﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class WireOrientation : ConductiveObject {

    public enum Orientations { topLeft, topRight, bottomLeft, bottomRight, horizontal, vertical }
    public Orientations orientation;

    void Update () {
        //Change rotation of wire sprite depending on which direction the wire is going in
        switch (orientation)
        {
            case Orientations.topLeft:
            case Orientations.vertical:
                transform.rotation = Quaternion.Euler(new Vector3(0, 0, 0));
                break;
            case Orientations.topRight:
                transform.rotation = Quaternion.Euler(Vector3.forward * 270);
                break;
            case Orientations.bottomRight:
                transform.rotation = Quaternion.Euler(Vector3.forward * 180);
                break;
            case Orientations.bottomLeft:
            case Orientations.horizontal:
                transform.rotation = Quaternion.Euler(Vector3.forward * 90);
                break;
        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Playtest_tracker : MonoBehaviour {

    private float time = 0.0f;
    private float startTime;
    private int numPowered = 0;
    private List<string> activatedCircuits = new List<string>();

    public GameObject player;
    public Circuit[] components;

    // Use this for initialization
    void Start () {
        startTime = Time.time;
	}

    // Update is called once per frame
    void Update() {
        float t = Time.time - startTime;
        float minutes = (t / 60);
        float seconds = (t % 60);
        //Debug.Log("Time = " + (int)minutes + ":" + seconds + ", Position = " + player.transform.position + ", " + checkCircuitActive());
        Debug.Log(checkCircuitActive());
    }

    string checkCircuitActive() {
        activatedCircuits.Clear();
        for (int i=0; i<components.Length; i++) {
            /*numPowered = 0;
            for (int j=0; j< components[i].wires.Length; j++) {
                if (components[i].wires[j].isPowered) {
                    numPowered++;
                }
            }*/
            if (components[i].circuitPowered) {
                //record it
                activatedCircuits.Add("Circuit " + i + " is active.");
            }
        }
        return string.Join(" & ", activatedCircuits.ToArray());
    }
}
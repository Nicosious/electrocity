﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CameraController : MonoBehaviour {

    public Transform player;
    public float leftBound;
    public float rightBound;
    public float topBound;
    public float bottomBound;
    public Slider volumeSlider;

    private float z_offset = 0.0f;
    bool lockX;
    bool lockY;

    // Use this for initialization
    void Start () {
        z_offset = this.transform.position.z;
        lockX = false;
        lockY = false;
        volumeSlider.value = 0.5f;
    }

    // Update is called once per frame
    void Update() {
        cameraBoundryCheck();
        volumeControl();
    }

    //checks to see where player is to determine camera boundry area.
    void cameraBoundryCheck() {
        //locks x-axis if player is within right or left bounds
        if (player.position.x < leftBound || player.position.x > rightBound) {
            lockX = true;
        } else {
            lockX = false;
        }
        // locks y-axis if player is within top or bottom bounds
        if (player.position.y < bottomBound || player.position.y > topBound) {
            lockY = true;
        } else {
            lockY = false;
        }

        //Camera movement logic based on defined bounds
        if (lockX && !lockY) {
            this.transform.position = new Vector3(transform.position.x, player.position.y, z_offset);           //camera locks x-axis
        } else if (!lockX && lockY) {
            this.transform.position = new Vector3(player.position.x, transform.position.y, z_offset);           //camera locks y-axis
        } else if (lockX && lockY) {
            this.transform.position = new Vector3(transform.position.x, transform.position.y, z_offset);        //camera locks both axis
        } else {
            this.transform.position = new Vector3(player.position.x, player.position.y, z_offset);              //camera locks no axis
        }
    }

    void volumeControl() {
        //Debug.Log(volumeSlider.value);
        AudioListener.volume = volumeSlider.value;
    }
}

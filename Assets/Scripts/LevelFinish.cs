﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LevelFinish : MonoBehaviour {

    public GameObject finishMenu;

	void Start () {
        finishMenu.SetActive(false);
    }

    void OnTriggerEnter2D(Collider2D col) {
        if (col.gameObject.tag == "Player") {
            finishMenu.SetActive(true);
            Time.timeScale = 0;
        }
    }
}

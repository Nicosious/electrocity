﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BlockController : MonoBehaviour {

    public EditingGridSnap gridSnap;
    public GameObject player;
    public bool EnableCircuitCurrent = false;

    Rigidbody2D RB2D;
    bool pushableSwitch = false;
    Vector3 _offsetPos;

    // Use this for initialization
    void Start () {
        gridSnap = GetComponent<EditingGridSnap>();
        RB2D = GetComponent<Rigidbody2D>();
        _offsetPos = transform.position - player.transform.position;                //offset between block & player
    }
	
	// Update is called once per frame
	void Update () {
        //generates the offset positions for the rays to cast from
        float xTemp = transform.position.x;
        float yTemp = transform.position.y;
        float zTemp = transform.position.z;
        Vector3 X_offset = new Vector3(xTemp + 1.2f, yTemp, zTemp);
        Vector3 Y_offset = new Vector3(xTemp, yTemp + 1.2f, zTemp);
        //Fires 2 rays across the block to detect player collision in a given direction
        RaycastHit2D y_hit = GenerateRay(Y_offset, transform.TransformDirection(Vector3.down), 2.4f, 1 << 8);
        RaycastHit2D x_hit = GenerateRay(X_offset, transform.TransformDirection(Vector3.left), 2.4f, 1 << 8);
        DragBox(x_hit, y_hit);
    }

    //Fires a raycast from origin at direction for a distance of range, layermask controls hich layers are acceptd or ignored. Modified from: https://answers.unity.com/questions/752147/how-can-i-change-a-variable-on-the-object-hit-by-a.html
    RaycastHit2D GenerateRay(Vector3 origin, Vector3 direction, float range, int layerMask) {
        RaycastHit2D ray = Physics2D.Raycast(origin, direction, range, layerMask);
        Debug.DrawRay(origin, direction * range, Color.cyan);
        return ray;
    }

    //Enables player to drag box along grid and snaps box to grid after release. Modified from: https://gamedev.stackexchange.com/questions/120410/make-object-follow-another-object-while-keeping-the-same-distance-from-it
    void DragBox(RaycastHit2D xRay, RaycastHit2D yRay) {
        //check if player is in correct range and control box pushability using boolean
        if (Input.GetMouseButtonDown(1) && ((xRay.collider != null && xRay.collider.gameObject.tag == "Player")||(yRay.collider != null && yRay.collider.gameObject.tag == "Player"))) {
            _offsetPos = transform.position - player.transform.position;        //set box offset position from players position
            pushableSwitch = true;
        }
        //On mouse release 
        if (Input.GetMouseButtonUp(1)) {
            pushableSwitch = false;
        }

        //box movement logic
        if (pushableSwitch && Input.GetMouseButton(1) && xRay.collider != null && xRay.collider.gameObject.tag == "Player"){       //player is alligned with boxes x-axis
            offsetCalculator(true, false);
            gridSnap.enabled = false;                                                                               //remove block snap
            //RB2D.constraints = RigidbodyConstraints2D.FreezePositionY | RigidbodyConstraints2D.FreezeRotation;      //lock all movement along y-axis
            RB2D.constraints = RigidbodyConstraints2D.FreezeRotation;                                                 //freeze rotation
            //player.GetComponent<Rigidbody2D>().constraints = RigidbodyConstraints2D.FreezePositionY;                //lock player to x-axis
            transform.position = player.transform.position + _offsetPos;    //moves block based on player's position
        }else if (pushableSwitch && Input.GetMouseButton(1) && yRay.collider != null && yRay.collider.gameObject.tag == "Player"){      //player is alligned with boxes y-axis
            offsetCalculator(false, true);
            gridSnap.enabled = false;                                                                               //remove block snap   
            //RB2D.constraints = RigidbodyConstraints2D.FreezePositionX | RigidbodyConstraints2D.FreezeRotation;      //lock all movement along x-axis
            RB2D.constraints = RigidbodyConstraints2D.FreezeRotation;                                                 //freeze rotation
            //player.GetComponent<Rigidbody2D>().constraints = RigidbodyConstraints2D.FreezePositionX;                //lock player to x-axis
            transform.position = player.transform.position + _offsetPos;    //moves block based on player's position
        }else{                                                                                                                          //player isn't alligned with either axis.
            offsetCalculator(false, false);
            gridSnap.enabled = true;                                                                //snap block to grid
            RB2D.constraints = RigidbodyConstraints2D.FreezeRotation;                               //reset constraints, only rotation is frozen
            //player.GetComponent<Rigidbody2D>().constraints = RigidbodyConstraints2D.None;           //player constraints removed
        }
    }

    //offsets box collider away from player to fix collision detection
    void offsetCalculator(bool X, bool Y) {
        if (X){                 //checks if player is in X axis
            GetComponent<Rigidbody2D>().bodyType = RigidbodyType2D.Dynamic;
            if (player.transform.position.x < transform.position.x) {
                GetComponent<BoxCollider2D>().offset = new Vector2(0.1f, 0.0f);
            } else if (player.transform.position.x > transform.position.x){
                GetComponent<BoxCollider2D>().offset = new Vector2(-0.1f, 0.0f);
            }
        }else if (Y){           //checks if player is in Y axis
            if (player.transform.position.y < transform.position.y){
                GetComponent<BoxCollider2D>().offset = new Vector2(0.0f, 0.1f);
            }else if (player.transform.position.y > transform.position.y){
                GetComponent<BoxCollider2D>().offset = new Vector2(0.0f, -0.1f);
            }
        } else {                //if no axis detected reset the blocks state & collider
            GetComponent<BoxCollider2D>().offset = new Vector2(0.0f, 0.0f);
        }
    }

    void OnCollisionEnter2D(Collision2D col) {
        pushableSwitch = false;
    }

    void OnCollisionExit2D(Collision2D col) {
        pushableSwitch = true;
    }

    void OnTriggerStay2D(Collider2D other) {
        if (other.gameObject.GetComponent<BlockTrigger>()) {
            EnableCircuitCurrent = other.gameObject.GetComponent<BlockTrigger>().isPowered;
        }
    }

    void OnTriggerExit2D(Collider2D other) {
        EnableCircuitCurrent = false;
    }
}
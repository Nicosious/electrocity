﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BlockTrigger : MonoBehaviour {
    public bool blockTouching = false;
    public bool isPowered;

    private void OnTriggerEnter2D(Collider2D collision) {
        if(collision.tag == "Block") {
            blockTouching = true;
        }
    }

    private void OnTriggerExit2D(Collider2D collision) {
        if (collision.tag == "Block") {
            blockTouching = false;
        }
    }
}

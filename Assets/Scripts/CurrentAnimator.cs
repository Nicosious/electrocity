﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Animator))]
[RequireComponent(typeof(SpriteRenderer))]
public class CurrentAnimator : MonoBehaviour {

    //public ConductiveObject parentConductor;
    public GameObject parentObject;
    private Animator anim;

    // Use this for initialization
    void Start () {
        anim = GetComponent<Animator>();
    }
	
	// Update is called once per frame
	void Update () {
        //matches the gameobject to the animation
        switch (componentCheck()) {
            case 0:         //default/null animation state
                Debug.Log("no animation");
                anim.SetInteger("AnimationState", 0);
                break;
            case 1:         //animation check for straight wires
                if (parentObject.GetComponent<WireOrientation>().isPowered) {
                    anim.SetInteger("AnimationState", componentCheck());
                } else {
                    anim.SetInteger("AnimationState", 0);
                }
                break;
            case 2:         //animation check for corner wires
                if (parentObject.GetComponent<WireOrientation>().isPowered) {
                    anim.SetInteger("AnimationState", componentCheck());
                } else {
                    anim.SetInteger("AnimationState", 0);
                }
                break;
            case 3:         //animation check for power supply
                if (parentObject.GetComponent<PowerSupplyController>().PowerEnabled) {
                    anim.SetInteger("AnimationState", componentCheck());
                } else {
                    anim.SetInteger("AnimationState", 0);
                }
                break;
            case 4:         //animation check for pushable block
                //check if block is in Block Trigger & that it's circuit is powered
                if (parentObject.GetComponent<BlockController>().EnableCircuitCurrent) {
                    anim.SetInteger("AnimationState", componentCheck());
                } else {
                    anim.SetInteger("AnimationState", 0);
                }
                break;
            case 5:         //animation check for port_end_left_wire 
                if (parentObject.GetComponent<WireOrientation>().isPowered) {
                    anim.SetInteger("AnimationState", componentCheck());
                } else {
                    anim.SetInteger("AnimationState", 0);
                }
                break;
        }
    }

    //Uses components to check which animation to apply
    public int componentCheck() {
        if (parentObject.transform.GetChild(0).gameObject.GetComponent<SpriteRenderer>() != null && parentObject.transform.GetChild(1).gameObject.GetComponent<SpriteRenderer>().sprite.name == "port_end_right_wire") {
            return 5;
        }else if (parentObject.GetComponent<WireOrientation>()) {
            if (parentObject.GetComponent<WireOrientation>().orientation == WireOrientation.Orientations.vertical || parentObject.GetComponent<WireOrientation>().orientation == WireOrientation.Orientations.horizontal) {
                return 1;
            } else {
                return 2;
            }
        } else if (parentObject.GetComponent<PowerSupplyController>()) {
            return 3;
        } else if (parentObject.GetComponent<BlockController>()) {
            return 4;
        } else {
            return 0;
        }
    }
}

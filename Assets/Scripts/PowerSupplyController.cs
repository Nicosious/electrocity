﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PowerSupplyController : MonoBehaviour {

    public bool PowerEnabled = false;
    public WireOrientation inputWire;

	// Use this for initialization
	void Start () {
        if (inputWire != null) {
            inputWire.GetComponent<WireOrientation>();
        }
    }
	
	// Update is called once per frame
	void Update () {
        if (PowerEnabled) {
            inputWire.setPowered(true);
        } else {
            inputWire.setPowered(false);
        }
	}

    //Returns an int to keep track of how many charges the player has 
    public int TogglePowerSupply(bool inRange, bool mouse, int numCharges) {
        if (inRange && PowerEnabled && mouse) {
            PowerEnabled = false; //The power supply is depowered
            return 1; //and the player gains a charge
        } else if (inRange && !PowerEnabled && mouse && numCharges > 0) {
            PowerEnabled = true; //The power supply is powered
            return -1; //and the player loses a charge
        }
        return 0; //No effect to the players charges
    }
}

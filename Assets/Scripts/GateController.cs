﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GateController : MonoBehaviour {

    //Connected Conductive Objects
    public ConductiveObject[] connectedObjs;
    public GameObject gateChild;

    public bool gatePowered;

    //Any time one of the gates connected objects changes state (powered to unpowered or nice versa) the gate also changes state
    //(powered to unpowered or vice versa)
    //This keeps track of if each connected object is powered so the gate can detect a change in the state of these objects
    public bool[] powered;

    //for making the gate play a clip when it is on
    private AudioSource audio;
    public AudioClip gateOff;

    void Start() {
        //get the required components
        audio = GetComponent<AudioSource>();

        if (!gatePowered) { //if the gate is not powered, turn its child (the electric current) off
            gateChild.SetActive(false);
        } else { //otherwise start its audio playing
            audio.Play();
        }

        //the initial status of the connected objects is stored in the gate so it can detect changes
        powered = new bool[connectedObjs.Length];
        for (int i = 0; i < connectedObjs.Length; i++) {
            if (connectedObjs[i] != null) {
                powered[i] = connectedObjs[i].isPowered;
            }
        }
    }

    // Update is called once per frame
    void Update() {
        checkCharge();
    }

    private void checkCharge() {
        for (int i = 0; i < powered.Length; i++) { //for each connected object this gate has
            if (connectedObjs[i] != null) { //if the object is not null, and
                if (connectedObjs[i].isPowered != powered[i]) { //If the state of this connected object has changed, the gate turns on/off
                    gateChild.SetActive(!gateChild.activeSelf); //The gate turns on if it was off, or off if it was on
                    gatePowered = !gatePowered; //update the gate powered variable
                    powered[i] = connectedObjs[i].isPowered; //and the gate's knowledge of its connected item is updated

                    if (gatePowered) { //if the gate is on
                        //play a sound if this audiosource is not already playing one
                        if (!audio.isPlaying) {
                            audio.Play();
                        }
                    } else {
                        audio.Stop();
                        audio.PlayOneShot(gateOff);
                    }
                }
            }
        }
    }
}

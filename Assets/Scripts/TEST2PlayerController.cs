﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TEST2PlayerController : MonoBehaviour {

    public float moveSpeed = 10.0f;
    public float pulseRange = 1.0f;
    public GameObject pulseBeam;
    private Rigidbody2D rb2D;
    private Vector2 mousePos;
    public int turnSpeed;
    private Quaternion turnRight = Quaternion.Euler(0, 0, -90);
    private Quaternion turnLeft = Quaternion.Euler(0, 0, 90);
    private Quaternion turnDown = Quaternion.Euler(0, 0, 0);
    private Quaternion turnUp = Quaternion.Euler(0, 0, 180);
    private Quaternion targetAngle;
    private float lookAngle;


    //for making the pulse play a clip when the player uses it
    private AudioSource audio;
    public AudioClip pulse;

    //For keeping track of the number of charges
    public Text chargesLabel;
    public int numCharges;

    // Use this for initialization
    void Start () {
        rb2D = GetComponent<Rigidbody2D>();
        audio = GetComponent<AudioSource>();
        pulseBeam.transform.position = new Vector3(transform.position.x, transform.position.y + (pulseRange / 2.0f), 0);
        pulseBeam.transform.localScale = new Vector3(1, pulseRange, 1);
        pulseBeam.SetActive(false);
    }
	
	// Update is called once per frame
	void FixedUpdate () {
        if (Time.timeScale != 0) {
            mouseCoordinateToWorldConverter();
            playerMovement();
            updateCharges();
        }
    }

    //moves player sprite & Rotates player based on mouse position. http://forum.brackeys.com/thread/2d-top-down-movement-bug-go-too-fast-diagonally/ 
    void playerMovement()
    {
        Vector2 movement = new Vector2(Input.GetAxisRaw("Horizontal") * Time.deltaTime, Input.GetAxisRaw("Vertical") * Time.deltaTime);
        Vector3 multiplier = new Vector2(moveSpeed, moveSpeed);

        rb2D.velocity = movement.normalized * multiplier;

            float angle = -Vector2.SignedAngle(mousePos, new Vector3(0,1,0));
            Quaternion faceMouse = Quaternion.Euler(0, 0, angle);
            transform.rotation = Quaternion.RotateTowards(transform.rotation, faceMouse, turnSpeed*Time.deltaTime);

        //check to see if charges exist on left mouse click
        if (numCharges > 0 && Input.GetMouseButton(0)) {
            EmitPulseBeam();            //emits the pulse sprite
        } else {
            pulseBeam.SetActive(false);     //deactivates pulse sprite on mouse release or if out of charges
        }
        FireRay(transform.position, transform.TransformDirection(Vector3.up), pulseRange, 1 << 8);

        if (Input.GetAxisRaw("Horizontal") == 1){
           transform.rotation = Quaternion.RotateTowards(transform.rotation, turnRight, turnSpeed*Time.deltaTime);
        }

        if(Input.GetAxisRaw("Horizontal") == -1){
            transform.rotation = Quaternion.RotateTowards(transform.rotation, turnLeft, turnSpeed*Time.deltaTime);
        }

        if(Input.GetAxisRaw("Vertical") == 1){
            transform.rotation = Quaternion.RotateTowards(transform.rotation, turnDown, turnSpeed*Time.deltaTime);
        }

        if(Input.GetAxisRaw("Vertical") == -1){
            transform.rotation = Quaternion.RotateTowards(transform.rotation, turnUp, turnSpeed*Time.deltaTime);
        }
        
    }


    void mouseCoordinateToWorldConverter()
    {
        mousePos = Input.mousePosition - Camera.main.WorldToScreenPoint(rb2D.position);
    }

    //Fires a raycast to detect & control powerSupply objects. Modified from: https://answers.unity.com/questions/752147/how-can-i-change-a-variable-on-the-object-hit-by-a.html
    void FireRay(Vector3 origin, Vector3 direction, float range, int layerMask){
        RaycastHit2D hit = Physics2D.Raycast(origin, direction, range, ~layerMask);
        if (hit.collider != null && hit.collider.gameObject.tag == "PowerSupply") {
            numCharges += hit.transform.GetComponent<PowerSupplyController>().TogglePowerSupply(true, Input.GetMouseButtonDown(0), numCharges);
        }
        Debug.DrawRay(origin, direction * range, Color.yellow);
        Debug.DrawRay(rb2D.position, mousePos, Color.red);
       
    }

    void EmitPulseBeam() {
        pulseBeam.SetActive(true);
        
        //stop any sounds the players audiosource is currently playing
        audio.Stop();
        //play a zap sound when the pulse activates
        audio.PlayOneShot(pulse);
    }

    void updateCharges() {
        chargesLabel.text = numCharges.ToString();
    }
}

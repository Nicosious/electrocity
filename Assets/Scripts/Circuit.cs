﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Circuit : MonoBehaviour {
    public PowerSupplyController[] powerSupply;
    public SubCircuit[] subCircuits;
    public BlockTrigger[] blockTrigger;
    private int numTriggersTouching = 0;
    private int numUnpowered = 0;
    bool powerSupplyOn = false;
    public bool circuitPowered = false;

    // Use this for initialization
    void Start() {
        subCircuits = GetComponentsInChildren<SubCircuit>();
    }

    // Update is called once per frame
    void Update() {
        checkPowered();
        numTriggersTouching = 0;

        for (int i = 0; i < blockTrigger.Length; i++) {
            if (blockTrigger[i] != null && blockTrigger[i].blockTouching) { //if there is a blockTrigger, and a block is colliding with that trigger
                numTriggersTouching++;
            }
        }

        if (powerSupplyOn) { //if at least one of the connected power supplies is on
            for (int i = 0; i <= blockTrigger.Length; i++) { //use number of blocks in slots to determine how many subcircuts are powered
                if (i <= numTriggersTouching) {
                    powerCircuit(i);
                    powerBlock(i);
                    circuitPowered = true;
                } else {
                    depowerCircuit(i);
                    depowerBlock(i);
                }
            }
        } else {
            depowerCircuits();
            depowerBlocks();
            circuitPowered = false;
        }
    }

    //Check if power supplies are powered
    void checkPowered() {
        numUnpowered = 0;
        for (int i = 0; i < powerSupply.Length; i++) {
            if (powerSupply[i] != null && !powerSupply[i].PowerEnabled) { //keeps track of how many power supplies are unpowered
                numUnpowered++;
            }
        }

        //if all power supplies are unpowered, the circuit is not being supplied power
        if (numUnpowered == powerSupply.Length) {
            powerSupplyOn = false;
        } else { //otherwise, the circuit is being supplied power (if one or more of the connected power supplies is powered)
            powerSupplyOn = true;
        }

    }

    //Powers a circuit
    void powerCircuit(int index) {
        subCircuits[index].powerWires();
    }

    //Depowers a single circuit
    void depowerCircuit(int index) {
            subCircuits[index].depowerWires();
    }

    //Depowers all circuits
    void depowerCircuits() {
        for (int i = 0; i < subCircuits.Length; i++) {
            subCircuits[i].depowerWires();
        }
    }

    //Sets the isPowered bool in a block trigger so you can tell if a block is powered
    void powerBlock(int index) {
        if (index > 0) {
            blockTrigger[index - 1].isPowered = true;
        }
    }

    //Depowers a single blockTrigger (blocks)
    void depowerBlock(int index) {
            blockTrigger[index - 1].isPowered = false;
    }

    //Depowers all blockTriggers (blocks)
    void depowerBlocks() {
        for (int i = 0; i < blockTrigger.Length; i++) {
            blockTrigger[i].isPowered = false;
        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TESTPlayerController : MonoBehaviour {

    public float moveSpeed = 10.0f;
    public float turnSpeed = 1000.0f;
    public float pulseRange = 1.0f;
    private Quaternion turnRight = Quaternion.Euler(0, 0, -90);
    private Quaternion turnLeft = Quaternion.Euler(0, 0, 90);
    private Quaternion turnDown = Quaternion.Euler(0, 0, 0);
    private Quaternion turnUp = Quaternion.Euler(0, 0, 180);
    private Rigidbody2D rb2D;
    public Text chargesLabel;
    public int numCharges;
    private Vector3 mousePos;

    // Use this for initialization
    void Start () {
        rb2D = GetComponent<Rigidbody2D>();
        updateCharges();
    }
	
	// Update is called once per frame
	void FixedUpdate () {
        //mouseCoordinateToWorldConverter();
        if (Time.timeScale != 0) {
            playerMovement();
            updateCharges();
        }
    }

    //moves player sprite & Rotates player based on mouse position. http://forum.brackeys.com/thread/2d-top-down-movement-bug-go-too-fast-diagonally/ 
    void playerMovement()
    {
        Vector2 movement = new Vector2(Input.GetAxisRaw("Horizontal") * Time.deltaTime, Input.GetAxisRaw("Vertical") * Time.deltaTime);
        Vector3 multiplier = new Vector2(moveSpeed, moveSpeed);

        rb2D.velocity = movement.normalized * multiplier;
        rb2D.constraints = RigidbodyConstraints2D.FreezeRotation;

	transform.LookAt(mousePos, Vector3.forward);

        //if(Input.GetAxisRaw("Horizontal") == 1){
         //   transform.rotation = Quaternion.RotateTowards(transform.rotation, turnRight, turnSpeed*Time.deltaTime);
        //}

        //if(Input.GetAxisRaw("Horizontal") == -1){
        //    transform.rotation = Quaternion.RotateTowards(transform.rotation, turnLeft, turnSpeed*Time.deltaTime);
        //}

        //if(Input.GetAxisRaw("Vertical") == 1){
        //    transform.rotation = Quaternion.RotateTowards(transform.rotation, turnDown, turnSpeed*Time.deltaTime);
        //}

        //if(Input.GetAxisRaw("Vertical") == -1){
        //    transform.rotation = Quaternion.RotateTowards(transform.rotation, turnUp, turnSpeed*Time.deltaTime);
        //}

        FireRay(transform.position, transform.TransformDirection(Vector3.up), pulseRange, 1<<8);
    }

    //Determines the current mouseposition in screen coordinates then convertes them to the game's world coordinates. Modified from: https://answers.unity.com/questions/540888/converting-mouse-position-to-world-stationary-came.html 
    void mouseCoordinateToWorldConverter()
    {
        mousePos = Input.mousePosition;
        mousePos = Camera.main.ScreenToWorldPoint(mousePos);
        mousePos.z = -500;
    }

    //Fires a raycast to detect & control powerSupply objects. Modified from: https://answers.unity.com/questions/752147/how-can-i-change-a-variable-on-the-object-hit-by-a.html
    void FireRay(Vector3 origin, Vector3 direction, float range, int layerMask){
        RaycastHit2D hit = Physics2D.Raycast(origin, direction, range, ~layerMask);
        if (hit.collider != null && hit.collider.gameObject.tag == "PowerSupply") {
            numCharges += hit.transform.GetComponent<PowerSupplyController>().TogglePowerSupply(true, Input.GetMouseButtonDown(0), numCharges);
        }
        Debug.DrawRay(origin, direction * range, Color.yellow);
    }

    void updateCharges() {
        chargesLabel.text = numCharges.ToString();
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class EditingGridSnap : MonoBehaviour {
    public float offsetX = 0;
    public float offsetY = 0;

    void Update () {

        float x, y;

        //Snap object to grid
        x = Mathf.Floor(transform.position.x) + offsetX;
        y = Mathf.Floor(transform.position.y) + offsetY;
        transform.position = new Vector3(x, y, 0);
    }
}

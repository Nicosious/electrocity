﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(HingeJoint2D))]

public class Door : MonoBehaviour {

    //so we can know if the door is powered or not
    public ConductiveObject connectedWire;
    public bool poweredOff;

    //for controlling the door opening and closing
    private HingeJoint2D hinge;
    private JointAngleLimits2D limits;
    private Rigidbody2D rigidbody;
    public float doorOpenLimit;
    public float doorClosedLimit;

    //for controlling whether the player can pass through the doors hitbox
    private BoxCollider2D collider;

    //for making the door play a clip when opened and closed
    private AudioSource audio;
    public AudioClip doorOpen;
    public AudioClip doorClose;

    //to stop the audio playing when a door starts open
    public bool startOpen;

    void Start () {
        //Get all attached components required
        hinge = GetComponent<HingeJoint2D>();
        rigidbody = GetComponent<Rigidbody2D>();
        collider = GetComponent<BoxCollider2D>();
        audio = GetComponent<AudioSource>();

        if (connectedWire != null) {
            poweredOff = !connectedWire.isPowered; //If there is a connected wire, set the doors powered status based on it
        }

        hinge.useMotor = true; //when the motor is on the door moves until it reaches its limit 

        //If the door is powered off set the limits to that of the closed door
        if (poweredOff) {
            limits.min = doorClosedLimit;
            limits.max = doorClosedLimit;
        } else { //Otherwise the initial limits are that of the open door
            limits.min = doorOpenLimit;
            limits.max = doorOpenLimit;
        }
        hinge.limits = limits;
        rigidbody.freezeRotation = true;
    }
	

	void Update () {
        if (connectedWire != null) {
            //if the connected wire is powered and the power of the door is off 
            if (connectedWire.isPowered && poweredOff) {   
                //unfreeze rotation so the door can move
                rigidbody.freezeRotation = false;

                //then open the door by changing the limits
                limits.min = doorOpenLimit;
                limits.max = doorOpenLimit;
                hinge.limits = limits;

                //stop any sounds this doors audiosource is currently playing
                audio.Stop();
                //play a sound when the door opens
                if (!startOpen) {
                    audio.PlayOneShot(doorOpen);
                } else {
                    startOpen = false;
                }

                //and udpate its powered status
                poweredOff = false;

                //allow the player to move through the door when open so it doesn't block areas off
                collider.enabled = false;
            } else if(!connectedWire.isPowered && !poweredOff){ //if the connected wire is unpowered and the door is powered
                //unfreeze rotation so the door can move
                rigidbody.freezeRotation = false;

                //then close it by changing the limits
                limits.min = doorClosedLimit;
                limits.max = doorClosedLimit;
                hinge.limits = limits;

                //stop any sounds this doors audiosource is currently playing
                audio.Stop();
                //play a sound when the door closes
                audio.PlayOneShot(doorClose);

                //and update its powered status
                poweredOff = true;

                //stop the player from moving through the door
                collider.enabled = true;
            }
        }

        //once the rotation of the door is complete (open or closed), stop it from moving any more
        if(hinge.jointAngle == limits.max) {
            rigidbody.freezeRotation = true;   
        }
    }
}

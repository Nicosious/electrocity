﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ConductiveObject : MonoBehaviour {

	public bool isPowered = false;

    public int numConnections;

    //Connected Conductive Objects
    public List<ConductiveObject> connectedObjs = new List<ConductiveObject>();

    void Update() {
        //Connected conductive objects are powered if this object is powered
        for (int i = 0; i < connectedObjs.Count; i++) {
            connectedObjs[i].setPowered(isPowered);
        }
    }

    public void setPowered(bool powered) {
        isPowered = powered;
    }

    protected void setConnected(ConductiveObject conductive) {
        if (connectedObjs.Count < numConnections) connectedObjs.Add(conductive);
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PauseMenu : MonoBehaviour {

    public GameObject shellPanel;
    private bool paused = false;

    void Start () {
        setPaused(paused);
	}
	
	void Update () {
        //pause if escape is pressed
        if (Input.GetKeyDown(KeyCode.Escape)) {
            if (!paused) {
                setPaused(true);
            } else {
                setPaused(false);
            }
        }
	}

    private void setPaused(bool pause) {
        //make the pause menu inactive when unpaused, active when paused
        paused = pause;
        shellPanel.SetActive(paused);
        Time.timeScale = paused ? 0 : 1;
    }

    public void continuePressed() {
        // resume the game
        setPaused(false);
    }

     public void restartPressed() {
        //restart the level
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }

    public void exitPressed() {
        // quit the game
        Application.Quit();
    }
}

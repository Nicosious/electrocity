﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PatrolGizmoDisplay : MonoBehaviour {

    public GameObject startPoint;
    public GameObject endPoint;

    // Use this for initialization
    void Start () {
		
	}

    void OnDrawGizmosSelected() {
        Gizmos.color = Color.green;
        Gizmos.DrawLine(startPoint.transform.position, endPoint.transform.position);
        Gizmos.DrawSphere(startPoint.transform.position, 0.1f);
        Gizmos.DrawSphere(endPoint.transform.position, 0.1f);
        Gizmos.color = Color.cyan;
    }
}
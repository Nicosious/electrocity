﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerController : MonoBehaviour {

    public float moveSpeed = 10.0f;
    public float pulseRange = 1.0f;
    public GameObject pulseBeam;
    private Rigidbody2D rb2D;
    private Vector3 mousePos;
    public Text chargesLabel;
    public int numCharges;

    // Use this for initialization
    void Start () {
        rb2D = GetComponent<Rigidbody2D>();
        pulseBeam.transform.position = new Vector3(transform.position.x, transform.position.y + (pulseRange / 2.0f), 0);
        pulseBeam.transform.localScale = new Vector3(1, pulseRange, 1);
        pulseBeam.SetActive(false);
    }
	
	// Update is called once per frame
	void FixedUpdate () {
        if (Time.timeScale != 0) {
            mouseCoordinateToWorldConverter();
            playerMovement();
            updateCharges();
        }
    }

    //moves player sprite & Rotates player based on mouse position. http://forum.brackeys.com/thread/2d-top-down-movement-bug-go-too-fast-diagonally/ 
    void playerMovement()
    {
        Vector2 movement = new Vector2(Input.GetAxisRaw("Horizontal") * Time.deltaTime, Input.GetAxisRaw("Vertical") * Time.deltaTime);
        Vector3 multiplier = new Vector2(moveSpeed, moveSpeed);

        rb2D.velocity = movement.normalized * multiplier;

	    transform.LookAt(mousePos, Vector3.forward);

        FireRay(transform.position, transform.TransformDirection(Vector3.up), pulseRange, 1 << 8);
        if (numCharges > 0) {
            EmitPulseBeam();
        }
    }

    //Determines the current mouseposition in screen coordinates then convertes them to the game's world coordinates. Modified from: https://answers.unity.com/questions/540888/converting-mouse-position-to-world-stationary-came.html 
    void mouseCoordinateToWorldConverter()
    {
        mousePos = Input.mousePosition;
        mousePos = Camera.main.ScreenToWorldPoint(mousePos);
        mousePos.z = -500;
    }

    //Fires a raycast to detect & control powerSupply objects. Modified from: https://answers.unity.com/questions/752147/how-can-i-change-a-variable-on-the-object-hit-by-a.html
    void FireRay(Vector3 origin, Vector3 direction, float range, int layerMask){
        RaycastHit2D hit = Physics2D.Raycast(origin, direction, range, ~layerMask);
        if (hit.collider != null && hit.collider.gameObject.tag == "PowerSupply") {
            numCharges += hit.transform.GetComponent<PowerSupplyController>().TogglePowerSupply(true, Input.GetMouseButtonDown(0), numCharges);
        }
        Debug.DrawRay(origin, direction * range, Color.yellow);
    }

    void EmitPulseBeam() {
        if (Input.GetMouseButton(0)) {
            pulseBeam.SetActive(true);
        } else {
            pulseBeam.SetActive(false);
        }
    }

    void updateCharges() {
        chargesLabel.text = numCharges.ToString();
    }
}
